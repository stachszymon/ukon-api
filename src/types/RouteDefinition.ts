type RouteDefinition = {
    path: string
    method: "get" | "post" | "delete" | "options" | "put" | "patch" | "param"
    handler: string | symbol
}

export default RouteDefinition
