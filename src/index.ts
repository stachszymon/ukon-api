import "reflect-metadata";
import {Connection, createConnection} from 'typeorm';
import App from "./App";
import koaLogger from "koa-logger";
import cors from "@koa/cors";
import bodyparser from "koa-bodyparser";
import errorHandler from "./lib/appErrorHandler";

import ProductController from "./controllers/ProductController";
import VendorController from "./controllers/VendorController";
import AssortmentController from "./controllers/AssortmentController";
import DeliveryController from "./controllers/DeliveryController";
import StatsController from "./controllers/StatsController";

createConnection()
    .then((connection: Connection) => {

        console.log('connected to database');

        const app: App = new App(3001);
        app.setMiddlewares([
            errorHandler,
            cors({
                origin: "*",
            }),
            koaLogger(),
            bodyparser(),
        ])
        app.initializeControllers([ProductController, VendorController, AssortmentController, DeliveryController, StatsController])

        app.run();

        // setTimeout(() => console.log(app.getRouter()), 1000);

    }).catch(err => {

    console.error("Could not connect to database", err)

})