import Koa from "koa";
import Router from "koa-router";
import RouteDefinition from "./types/RouteDefinition";

export default class App {
    private readonly app: Koa;
    private readonly router: Router;
    private port: number;
    private middlewares: any[];

    constructor(port: number = 3000) {
        this.app = new Koa();
        this.router = new Router();
        this.port = port;
    }

    public run(): App {
        this.middlewares.forEach(m => this.app.use(m));

        this.app.use(this.router.allowedMethods()).use(this.router.routes());

        this.app.listen(this.port, () => {
            console.log(`Koa app started on port: ${this.port}`)
        })
        return this;
    }

    public initializeControllers(controllers: any[]): void {
        const router = new Router();

        controllers.forEach(controller => {
            const instance = new controller(),
                prefix: string = Reflect.get(controller, "prefix"),
                routes: Array<RouteDefinition> = Reflect.get(controller, "routes");

            routes.forEach(route => {
                const method = instance[route.handler].bind(instance);
                if (route.method === 'param') router.param(route.path, method);
                else router[route.method](prefix + route.path, method);
            })
        })

        this.setRoutes(router);
    }

    public setMiddlewares(middlewares: any[]): void {
        this.middlewares = middlewares;
    }

    public setRoutes(router: Router) {
        const r: Router = this.getRouter();

        r.use(router.routes()).use(router.allowedMethods());
        // this.getApp().use(r.routes()).use(r.allowedMethods());
    }

    public getApp(): Koa {
        return this.app;
    }

    public getRouter(): Router {
        return this.router;
    }
}