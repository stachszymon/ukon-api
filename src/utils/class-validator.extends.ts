import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function IsNip(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            name: 'isNip',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [],
            options: validationOptions,
            validator: {
                validate(nip: any, args: ValidationArguments) {
                    if (nip == null) return false;

                    nip = String(nip).trim();

                    const nipWithoutDashes = nip.replace(/-/g, "");
                    const reg = /^[0-9]{10}$/;
                    if (reg.test(nipWithoutDashes) === false) {
                        return false;
                    } else {
                        const digits = ("" + nipWithoutDashes).split("");
                        const checksum =
                            (6 * parseInt(digits[0]) +
                                5 * parseInt(digits[1]) +
                                7 * parseInt(digits[2]) +
                                2 * parseInt(digits[3]) +
                                3 * parseInt(digits[4]) +
                                4 * parseInt(digits[5]) +
                                5 * parseInt(digits[6]) +
                                6 * parseInt(digits[7]) +
                                7 * parseInt(digits[8])) %
                            11;

                        if (parseInt(digits[9]) !== checksum) return false;
                    }

                    return true;
                },
            },
        });
    };
}