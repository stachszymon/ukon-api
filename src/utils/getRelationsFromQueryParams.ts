export default function (withModel: string | string[], workingRelations: string[]): string[] {
    const relations: string[] = [];

    if (withModel) {

        if (Array.isArray(withModel)) {
            withModel.forEach(item => workingRelations.includes(item) && relations.push(item))
        } else {
            if (workingRelations.includes(withModel)) relations.push(withModel)
        }
    }

    return relations
}