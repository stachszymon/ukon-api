import { validate, ValidationError as VE, ValidatorOptions } from "class-validator";

type validationErrors = { [key: string]: string[] };

export class ValidationError extends Error {
    public errors: validationErrors;

    constructor(errors: VE[]) {

        const reducedErrors = errors.reduce((sum: validationErrors, cur: VE) => {

            if (cur.constraints) {
                for (const [key, value] of Object.entries(cur.constraints)) {
                    if (sum[cur.property] == null) sum[cur.property] = [];

                    sum[cur.property].push(value);
                }
            }

            return sum;
        }, {} as validationErrors)

        let errorsString: string = "";

        for (const [key, value] of Object.entries(reducedErrors)) {
            errorsString += value.join(", ");
        }

        super(errorsString)

        this.errors = reducedErrors;
    }
}

export default async function (data: object, option?: ValidatorOptions): Promise<boolean> {
    const result = await validate(data, option);

    if (result.length === 0) {
        return true
    } else {
        throw new ValidationError(result)
    }
}