import {Context, Next} from 'koa';
import {getRepository, Repository} from 'typeorm';
import {Controller, Get, Delete, Param, Patch, Post} from "../lib/ControllerDecorators";
import Product from "../entities/Product";
import getRelationsFromQueryParams from "../utils/getRelationsFromQueryParams";
import validate from "../utils/validate";

@Controller("/product")
export default class ProductController {

    private productRepository: Repository<Product>;

    constructor() {
        this.productRepository = getRepository(Product);
    }

    @Param("product")
    public async productParam(param: string, ctx: Context, next: Next) {

        const relations: string[] =
            getRelationsFromQueryParams(ctx.query.withModel, ['assortment', 'vendor']);

        const where: { [key: string]: string } = {}

        if (ctx.query.assortment) where.assortment = ctx.query.assortment;
        if (ctx.query.vendor) where.vendor = ctx.query.vendor;

        ctx.state.product =
            await this.productRepository.findOne(param, {relations, where});

        if (ctx.state.product) await next()
        else ctx.status = 404
    }


    @Get("/")
    public async getAll(ctx: Context, next: Next) {

        const relations: string[] =
            getRelationsFromQueryParams(ctx.query.withModel, ['assortment', 'vendor']);

        const where: { [key: string]: string } = {}

        if (ctx.query.assortment) where.assortment = ctx.query.assortment;
        if (ctx.query.vendor) where.vendor = ctx.query.vendor;

        const res = await this.productRepository.find({
            relations,
            where,
            loadRelationIds: true,
        })

        ctx.body = res;
    }

    @Get("/:product/")
    public async getOne(ctx: Context, next: Next) {
        ctx.body = ctx.state.product;
    }

    @Post("/")
    public async create(ctx: Context, next: Next) {

        const assortment = this.productRepository.create(ctx.request.body)

        await validate(assortment)
        await this.productRepository.save(assortment)

        ctx.body = assortment;
        ctx.status = 201;

    }

    @Patch("/:product/")
    public async update(ctx: Context, next: Next) {

        const product: Product = ctx.state.product,
            updatedProduct: Product = this.productRepository.merge(product, ctx.request.body);

        await validate(updatedProduct);
        await this.productRepository.save(updatedProduct)

        ctx.status = 200;
        ctx.body = updatedProduct
    }

    @Delete("/:product/")
    public async delete(ctx: Context, next: Next) {
        await this.productRepository.delete(ctx.state.product);
        ctx.status = 204
    }

}