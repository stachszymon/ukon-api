import { Context, Next } from 'koa';
import { getRepository, Repository } from 'typeorm';
import { Controller, Get, Post, Delete, Patch, Param } from "../lib/ControllerDecorators";
import { Assortment } from './../entities/Assortment';
import validate from "../utils/validate";
import getRelationsFromQueryParams from "../utils/getRelationsFromQueryParams";

@Controller("/assortment")
export default class AssortmentController {

    private assortmentRepository: Repository<Assortment>;

    constructor() {
        this.assortmentRepository = getRepository(Assortment);
    }

    @Param("assortment")
    public async assortmentParam(param: string, ctx: Context, next: Next) {

        const relations: string[] = getRelationsFromQueryParams(ctx.query.withModel, ['products']);

        ctx.state.assortment = await this.assortmentRepository.findOne(param, { relations });

        if (ctx.state.assortment) await next()
        else ctx.status = 404
    }

    @Get("/")
    public async getAll(ctx: Context, next: Next) {

        const relations: string[] = getRelationsFromQueryParams(ctx.query.withModel, ['products'])

        const res = await this.assortmentRepository.find({ relations });

        ctx.body = res;
    }

    @Get("/:assortment/")
    public async getOne(ctx: Context, next: Next) {
        ctx.body = ctx.state.assortment;
    }

    @Post("/")
    public async create(ctx: Context, next: Next) {

        const assortment = this.assortmentRepository.create(ctx.request.body)

        await validate(assortment)
        await this.assortmentRepository.save(assortment)

        ctx.body = assortment;
        ctx.status = 201;

    }

    @Patch("/:assortment/")
    public async update(ctx: Context, next: Next) {

        const assortment: Assortment = ctx.state.assortment,
            updatedAssortment: Assortment = this.assortmentRepository.merge(assortment, ctx.request.body);

        await validate(updatedAssortment);
        await this.assortmentRepository.save(updatedAssortment)

        ctx.status = 200;
        ctx.body = updatedAssortment
    }

    @Delete("/:assortment/")
    public async delete(ctx: Context, next: Next) {
        await this.assortmentRepository.delete(ctx.state.assortment);
        ctx.status = 204
    }

}
