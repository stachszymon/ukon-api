import { Context, Next } from 'koa';
import { getRepository, Repository } from 'typeorm';
import { Controller, Delete, Get, Param, Post, Patch } from "../lib/ControllerDecorators";
import getRelationsFromQueryParams from "../utils/getRelationsFromQueryParams";
import validate from "../utils/validate";
import Vendor from "../entities/Vendor";
import VendorContact from '../entities/VendorContact';

@Controller("/vendor")
export default class VendorController {

    private vendorRepository: Repository<Vendor>;
    private contactRepository: Repository<VendorContact>;

    constructor() {
        this.vendorRepository = getRepository(Vendor);
        this.contactRepository = getRepository(VendorContact);
    }

    @Param("vendor")
    public async vendorParam(param: string, ctx: Context, next: Next) {

        const relations: string[] = getRelationsFromQueryParams(ctx.query.withModel, ['vendorContacts', 'products']);

        ctx.state.vendor = await this.vendorRepository.findOne(param, { relations });

        if (ctx.state.vendor) await next()
        else ctx.status = 404
    }

    @Get("/")
    public async getAll(ctx: Context, next: Next) {

        const relations: string[] = getRelationsFromQueryParams(ctx.query.withModel, ['vendorContacts', 'products']);

        const res = await this.vendorRepository.find({ relations })

        ctx.body = res;
    }

    @Get("/:vendor/")
    public async getOne(ctx: Context, next: Next) {
        ctx.body = ctx.state.vendor;
    }

    @Post("/")
    public async create(ctx: Context, next: Next) {

        const vendor = this.vendorRepository.create(ctx.request.body)

        await validate(vendor)
        await this.vendorRepository.save(vendor)

        ctx.body = vendor;
        ctx.status = 201;

    }

    @Patch("/:vendor/")
    public async update(ctx: Context, next: Next) {

        const vendor: Vendor = ctx.state.vendor,
            updatedVendor: Vendor = this.vendorRepository.merge(vendor, ctx.request.body);

        await validate(updatedVendor);
        await this.vendorRepository.save(updatedVendor)

        ctx.status = 200;
        ctx.body = updatedVendor
    }

    @Delete("/:vendor/")
    public async delete(ctx: Context, next: Next) {
        await this.vendorRepository.delete(ctx.state.vendor);
        ctx.status = 204
    }

    /**
     * VendorContact
     */
    @Param("vendorcontact")
    public async vendorContactParam(param: string, ctx: Context, next: Next) {

        ctx.state.vendorContact = await this.contactRepository.findOne(param);

        if (ctx.state.vendorContact) await next()
        else ctx.status = 404
    }

    @Post("/:vendor/vendorcontact")
    public async createContact(ctx: Context, next: Next) {

        const contact = this.contactRepository.create({ ...ctx.request.body, vendor: ctx.state.vendor.id });

        await validate(contact);
        await this.contactRepository.save(contact);

        ctx.body = contact;
        ctx.status = 201;
    }

    @Patch("/:vendor/vendorcontact/:vendorcontact")
    public async updateContact(ctx: Context, next: Next) {

        const contact: VendorContact = ctx.state.vendorContact,
            updatedContact: VendorContact = this.contactRepository.merge(contact, ctx.request.body);

        await validate(updatedContact);
        await this.contactRepository.save(updatedContact);

        ctx.status = 200;
        ctx.body = updatedContact;
    }

    @Delete("/:vendor/vendorcontact/:vendorcontact")
    public async deleteContact(ctx: Context, next: Next) {
        await this.contactRepository.delete(ctx.state.vendorContact);
        ctx.status = 204;
    }

}