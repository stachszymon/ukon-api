import {Controller, Delete, Get, Param, Post, Patch} from "../lib/ControllerDecorators";
import {Context, Next} from "koa";
import {getRepository, Repository} from "typeorm";
import Delivery from "../entities/Delivery";
import DeliveryItem from "../entities/DeliveryItem";

@Controller("/stats")
export default class VendorController {

    private deliveryRepository: Repository<Delivery>
    private itemRepository: Repository<DeliveryItem>

    constructor() {
        this.deliveryRepository = getRepository(Delivery);
        this.itemRepository = getRepository(DeliveryItem);
    }

    @Get("/sumDeliveries")
    public async sumDeliveries(ctx: Context, next: Next) {

        const data = [
            {
                name: 'Page A',
                uv: 4000,
                pv: 2400,
                amt: 2400,
            },
            {
                name: 'Page B',
                uv: 3000,
                pv: 1398,
                amt: 2210,
            },
            {
                name: 'Page C',
                uv: 2000,
                pv: 9800,
                amt: 2290,
            },
            {
                name: 'Page D',
                uv: 2780,
                pv: 3908,
                amt: 2000,
            },
            {
                name: 'Page E',
                uv: 1890,
                pv: 4800,
                amt: 2181,
            },
            {
                name: 'Page F',
                uv: 2390,
                pv: 3800,
                amt: 2500,
            },
            {
                name: 'Page G',
                uv: 3490,
                pv: 4300,
                amt: 2100,
            },
        ];

        ctx.body = data;
    }

}