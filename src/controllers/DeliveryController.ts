import {Context, Next} from 'koa';
import {Controller, Get, Delete, Param, Patch, Post} from "../lib/ControllerDecorators";
import Delivery from "../entities/Delivery";
import {getRepository, Repository} from "typeorm";
import LackOfQueryParamError from "../Erros/LackOfQueryParamError";
import validate from "../utils/validate";
import getRelationsFromQueryParams from "../utils/getRelationsFromQueryParams";
import DeliveryItem from "../entities/DeliveryItem";

@Controller("/delivery")
export default class DeliveryController {

    private deliveryRepository: Repository<Delivery>
    private itemRepository: Repository<DeliveryItem>

    constructor() {
        this.deliveryRepository = getRepository(Delivery)
        this.itemRepository = getRepository(DeliveryItem);
    }

    @Param("delivery")
    public async deliveryParam(param: string, ctx: Context, next: Next) {
        const relations: string[] = getRelationsFromQueryParams(ctx.query.withModel, []);

        const where: { [key: string]: string } = {}

        if (ctx.query.vendor) where.vendor = ctx.query.vendor;

        ctx.state.delivery = await this.deliveryRepository.findOne(param, {relations, where});

        if (ctx.state.delivery) await next();
        else ctx.status = 404;
    }

    @Get("/")
    public async getAll(ctx: Context, next: Next) {

        const {
            vendor: vendorParam
        } = ctx.query;

        if (vendorParam == null) {
            throw new LackOfQueryParamError("Vendor param required")
        }

        const where: { [key: string]: string } = {}

        if (vendorParam) where.vendor = vendorParam;

        const res = await this.deliveryRepository.find({where})

        ctx.body = res;
    }

    @Get("/:delivery/")
    public async getOne(ctx: Context, next: Next) {
        ctx.body = ctx.state.delivery;
    }

    @Post("/")
    public async create(ctx: Context, next: Next) {

        const delivery = this.deliveryRepository.create(ctx.request.body);

        await validate(delivery);
        await this.deliveryRepository.save(delivery);

        ctx.body = delivery;
        ctx.status = 201;

    }

    @Patch("/:delivery/")
    public async update(ctx: Context, next: Next) {
        const delivery: Delivery = ctx.state.delivery,
            updatedDelivery: Delivery = this.deliveryRepository.merge(delivery, ctx.request.body);

        await validate(updatedDelivery);
        await this.deliveryRepository.save(updatedDelivery)

        ctx.status = 200
        ctx.body = updatedDelivery

    }

    @Delete("/:delivery/")
    public async delete(ctx: Context, next: Next) {
        await this.deliveryRepository.delete(ctx.state.delivery);
        ctx.status = 204;
    }

    //DeliveryItem Logic

    @Param("deliveryItem")
    public async getItem(param: string, ctx: Context, next: Next) {
        ctx.state.deliveryItem = await this.itemRepository.findOne(param, {
            where: {
                delivery: ctx.state.delivery.id
            },
            loadRelationIds: true,
        })

        if (ctx.state.deliveryItem) await next()
        else ctx.status = 404
    }

    @Get("/:delivery/item")
    public async getItems(ctx: Context, next: Next) {

        const items = await this.itemRepository.find({
            where: {
                delivery: ctx.state.delivery.id,
            },
            loadRelationIds: true,
        })

        if (items) {
            ctx.body = items;
            ctx.status = 200;
        }
    }

    @Post("/:delivery/item")
    public async createItem(ctx: Context, next: Next) {
        const item = this.itemRepository.create({...ctx.request.body, delivery: ctx.state.delivery.id})

        await validate(item)
        await this.itemRepository.save(item)

        ctx.body = item;
        ctx.status = 201
    }

    @Patch("/:delivery/item/:deliveryItem/")
    public async updateItem(ctx: Context, next: Next) {
        const item: DeliveryItem = ctx.state.deliveryItem,
            updatedItem: DeliveryItem = this.itemRepository.merge(item, ctx.request.body);

        await validate(updatedItem)
        await this.itemRepository.save(updatedItem)

        ctx.status = 200
        ctx.body = updatedItem
    }

    @Delete("/:delivery/item/:deliveryItem/")
    public async deleteItem(ctx: Context, next: Next) {
        await this.itemRepository.delete(ctx.state.deliveryItem);
        ctx.status = 204
    }
}