import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToOne} from "typeorm";
import {IsNotEmpty, Length, Min, Max, IsOptional, IsInt} from "class-validator"
import Product from "./Product";
import Delivery from "./Delivery"

@Entity()
export default class DeliveryItem {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, type: "int"})
    @IsNotEmpty()
    @Min(1)
    quantity: number;

    @Column({nullable: false, type: "int"})
    @IsNotEmpty()
    @Min(1)
    price: number;

    @ManyToOne(() => Product, product => product.id, {nullable: false, onDelete: "CASCADE"})
    @IsNotEmpty()
    @IsInt()
    product: Product;

    @ManyToOne(() => Delivery, delivery => delivery.id, {nullable: false, onDelete: "CASCADE"})
    @IsNotEmpty()
    @IsInt()
    delivery: Delivery
}