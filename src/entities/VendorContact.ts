import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import Vendor from "./Vendor";

//TODO VALIDATION
@Entity()
export default class VendorContact {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    phoneNumber: string;

    @Column()
    email: string;

    @Column()
    description: string;

    @ManyToOne(() => Vendor, vendor => vendor.id, { nullable: false, onDelete: "CASCADE" })
    vendor: Vendor;

}