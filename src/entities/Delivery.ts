import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany} from "typeorm";
import { IsNotEmpty, Length, IsOptional, IsInt } from "class-validator"
import Vendor from "./Vendor";
import DeliveryItem from "./DeliveryItem";

@Entity()
export default class Delivery {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false, unique: true, length: 12 })
    @IsNotEmpty()
    @Length(3, 12)
    name: string;

    @Column({ nullable: false, type: "date" })
    @IsNotEmpty()
    date: Date;

    @ManyToOne(() => Vendor, vendor => vendor.id, { nullable: false, onDelete: "CASCADE" })
    @IsNotEmpty()
    @IsInt()
    vendor: Vendor;

    @OneToMany(() => DeliveryItem, deliveryItem => deliveryItem.id, { nullable: true, onDelete: "CASCADE"})
    deliveryItem: DeliveryItem[];

}