import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { IsNotEmpty, Length } from "class-validator"
import Product from './Product';

@Entity()
export class Assortment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false, unique: true, length: 255 })
    @IsNotEmpty()
    @Length(3, 255)
    name: string;

    @OneToMany(() => Product, product => product.assortment, { onDelete: "CASCADE" })
    products: Product[];
}