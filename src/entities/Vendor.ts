import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { IsNotEmpty, IsPostalCode, Length, IsOptional } from "class-validator"
import { IsNip } from "../utils/class-validator.extends";
import VendorContact from "./VendorContact";
import Product from "./Product";

@Entity()
export default class Vendor {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true, nullable: false, length: 12 })
    @IsNotEmpty()
    @Length(3, 12)
    code: string;

    @Column({ unique: true, nullable: false, length: 255 })
    @IsNotEmpty()
    @Length(3, 255)
    name: string;

    @Column({ nullable: true })
    @IsOptional()
    @Length(0, 255)
    address: string;

    @Column({ nullable: true })
    @IsOptional()
    @IsPostalCode("PL")
    postcode: string;

    @Column({ nullable: false, unique: true })
    @IsNotEmpty()
    @IsNip({ message: "Incorrect NIP value" })
    nip: string;

    @Column({ length: 255, nullable: true })
    @IsOptional()
    @Length(3, 255)
    companyName: string;

    @OneToMany(() => VendorContact, vendorContact => vendorContact.vendor, { onDelete: "CASCADE" })
    vendorContacts: VendorContact[];

    @OneToMany(() => Product, product => product.vendor, {})
    products: Product[];

}