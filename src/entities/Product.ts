import { Assortment } from './Assortment';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { IsNotEmpty, Length, Min, Max, IsOptional, IsInt } from "class-validator"
import Vendor from "./Vendor";

@Entity()
export default class Product {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false, unique: true, length: 12 })
    @IsNotEmpty()
    @Length(3, 12)
    name: string;

    @Column({ nullable: true, length: 255 })
    @IsOptional()
    @Length(3, 255)
    producent: string;

    @Column({ nullable: true, length: 255 })
    @IsOptional()
    @Length(3, 255)
    description: string

    @Column({ nullable: false })
    @IsNotEmpty()
    @IsInt()
    @Min(1)
    @Max(99)
    tax: number

    @ManyToOne(() => Vendor, vendor => vendor.id, { nullable: false, onDelete: "CASCADE" })
    @IsNotEmpty()
    @IsInt()
    vendor: Vendor;

    @ManyToOne(() => Assortment, assortment => assortment.id, { nullable: true, onDelete: "CASCADE" })
    @IsOptional()
    @IsNotEmpty()
    @IsInt()
    assortment: Assortment
}