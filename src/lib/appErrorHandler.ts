import {ValidationError} from './../utils/validate';
import {Context, Next} from "koa";
import {QueryFailedError} from 'typeorm';
import LackOfQueryParamError from "../Erros/LackOfQueryParamError";

interface QueryFailedErrorWithCode extends QueryFailedError {
    code: string;
    sqlMessage: string;
}

export default async function errorHandler(ctx: Context, next: Next) {
    try {
        await next();
    } catch (err) {
        console.error(err);

        if (err instanceof LackOfQueryParamError) {
            ctx.status = 400
            ctx.body = {errors: err.message}
        } else if (err instanceof ValidationError) {
            ctx.status = 400;
            ctx.body = {errors: err.errors};
        } else if (err as QueryFailedErrorWithCode instanceof QueryFailedError) {

            if (err?.code === "ER_DUP_ENTRY") {
                ctx.status = 400;
                ctx.body = {errors: err?.sqlMessage}
            } else if (err?.code === "ER_NO_REFERENCED_ROW_2") {
                ctx.status = 404;
                ctx.body = {errors: "Database relation not found"}
            }

        } else {
            ctx.status = 500
        }
    }
}