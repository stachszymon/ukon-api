FROM node:14

WORKDIR /usr/src/app

COPY package.json package-lock.json ./
RUN npm install

COPY . ./

#ADD . /usr/src/app
RUN npm run compile

CMD [ "node", "." ]
EXPOSE 3001

#COPY . .
#RUN npm run compile
#EXPOSE 3001
#CMD ["node", "."]